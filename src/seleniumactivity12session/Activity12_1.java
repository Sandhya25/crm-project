package seleniumactivity12session;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity12_1 {

	public static void main(String[] args) {
	
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://www.training-support.net/selenium/iframes");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is :" +pagetitle);
		
		driver.switchTo().frame(0);
		
		 WebElement frameHeading1 = driver.findElement(By.cssSelector("div.content"));
	     System.out.println(frameHeading1.getText());
		
		WebElement buttontext = driver.findElement(By.xpath("//button[@id='actionButton']"));
		
		System.out.println("The button text is:" +buttontext.getText());
		System.out.println("The button text is:" +buttontext.getCssValue("color"));
		
		driver.switchTo().defaultContent();
		
			
		
		
		
		

	}

}
