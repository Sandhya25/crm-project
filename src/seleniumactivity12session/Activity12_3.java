package seleniumactivity12session;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity12_3 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		// WebDriverWait wait = new WebDriverWait(driver, 10);
        Actions builder = new Actions(driver);
        
        
		driver.get("https://www.training-support.net/selenium/popups");
		
		System.out.println("The page title is : +driver.getTitle()");
		
		WebElement clickbutton = driver.findElement(By.cssSelector("button.ui.huge.inverted.orange.button"));
		
		
		//Hover over button
		
		builder.moveToElement(clickbutton).pause(Duration.ofSeconds(1)).build().perform();
        String tooltipText = clickbutton.getAttribute("data-tooltip");
        System.out.println("Tooltip text: " + tooltipText);
        
        
		clickbutton.click();
		
		//Wait for modal to appear
        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("modal")));
		
		driver.findElement(By.id("username")).sendKeys("admin");
		driver.findElement(By.id("password")).sendKeys("password");
		
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		
		String message = driver.findElement(By.id("action-confirmation")).getText();
			
		System.out.println(" + message");
		
		
		
		

	}

}
