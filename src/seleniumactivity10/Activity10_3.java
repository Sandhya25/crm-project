package seleniumactivity10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Activity10_3 {

	public static void main(String[] args) {
	
		WebDriver driver = new FirefoxDriver();
		driver.get("https://training-support.net/selenium/drag-drop");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is :" +pagetitle);
		
		Actions actions = new Actions(driver);
		
		WebElement ballfinding = driver.findElement(By.id("draggable"));
		WebElement drop1 = driver.findElement(By.id("droppable"));
		WebElement drop2 = driver.findElement(By.id("dropzone2"));
		
		actions.dragAndDrop(ballfinding, drop1).build().perform();
		
		System.out.println("ENTERED DROPZONE 1");
		
		actions.dragAndDrop(ballfinding, drop2).build().perform();
		
		System.out.println("ENTERED DROPZONE 2");
		
	
		
		
		
		
		

	}

}
