package seleniumactivity11;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity11_3 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://www.training-support.net/selenium/javascript-alerts");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is :" +pagetitle);
		
		driver.findElement(By.xpath("//button[@id = 'prompt']")).click();
		
		Alert promptAlert = driver.switchTo().alert();
		
		String alerttile = promptAlert.getText();
		System.out.println("the page title is :" +alerttile);
		
		promptAlert.sendKeys("Hai");
		System.out.println("the new page title is :" +alerttile);
		
		
		promptAlert.accept();
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
