package seleniumactivity11;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity11_2 {

	public static void main(String[] args) { 
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://www.training-support.net/selenium/javascript-alerts");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is :" +pagetitle);
		
		driver.findElement(By.cssSelector("button#confirm")).click();
		
		Alert Confirmation = driver.switchTo().alert();
		
		String alerttest = Confirmation.getText();
		System.out.println("The alert test is :" +alerttest);
		
		Confirmation.accept();
		
		
		
		driver.findElement(By.cssSelector("button#confirm")).click();
	   Confirmation = driver.switchTo().alert();
		
		Confirmation.dismiss();
			
				

	

}
