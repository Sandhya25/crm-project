package suiteCRMproject;

import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import org.testng.annotations.BeforeClass;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;

public class AllProducts {
	WebDriver driver;
	
	   @BeforeClass(alwaysRun = true)
	    public void beforeClass() {
		   
		 //Create a new instance of the firefox driver
		     driver = new FirefoxDriver();
		     
		   //Open browser  
		   driver.get("https://alchemy.hguy.co/crm/");
		 }

		
	    @Test 
		
	    public void activity4credentials() {
			  
			 driver.findElement(By.id("user_name")).sendKeys("admin");
			 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
			 
			 driver.findElement(By.id("bigbutton")).click();
			 
			// WebElement homepage = driver.findElement(By.id("grouptab_0"));
			 
			//Assert.assertEquals("SALES",homepage.getText());
					  
		  }
	    
	    @Test (dependsOnMethods = {"activity4credentials"})
	    
	    public void Activity13() throws InterruptedException, IOException, CsvException {         
			 Thread.sleep(2000);
			 
		WebElement all = driver.findElement(By.xpath("//*[@id='grouptab_5']"));
			  Actions action = new Actions(driver);
			  
			  Thread.sleep(4000);
			  
			 action.moveToElement(all).click().build().perform();
			 
			 Thread.sleep(4000);
			 
			driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[7]/span[2]/ul/li[25]/a")).click();
			Thread.sleep(7000);
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div[1]/ul/li[1]/a/div[2]")).click();
		
			CSVReader reader = new CSVReader(new FileReader("src/resources/sample1.csv")); 
					
			List<String[]> list = reader.readAll();
	        System.out.println("Total number of rows are: " + list.size());
	        
	      //Create Iterator reference
	        Iterator<String[]> itr = list.iterator();

	        //Iterate all values
	        while(itr.hasNext()) {
	            String[] str = itr.next();

	            System.out.print("Values are: ");
	        
//	        for(int i=0;i<str.length;i++){
	        	String PName = str[0];
	        	String PartNumber =str[1];
	        	String Cost =str[2];
	        	String Price =str[3];
	        	
	        	System.out.println("product name is :" +PName);
	        	System.out.println("product name is :" +PartNumber);
	        	System.out.println("product name is :" +Cost);
	        	System.out.println("product name is :" +Price);
	        	
	        	Thread.sleep(7000);
	        	
	        	
	        	driver.findElement(By.xpath("//*[@id='name']")).sendKeys(PName);
	        	driver.findElement(By.id("part_number")).sendKeys(PartNumber);
	        	driver.findElement(By.id("cost")).sendKeys(Cost);
	        	driver.findElement(By.id("price")).sendKeys(Price);
	        	
	        	Thread.sleep(3000);
	        	
	        	driver.close();
	        	
	        	
	        	// driver.findElement(By.xpath("//button[@id='save_and_continue']")).click();
	        
	        
	        }
	        }
	    
  // @AfterClass
  public void afterClass() {
  }

}
