package suiteCRMproject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class HomePage {
	
	WebDriver driver;
	
	   @BeforeClass(alwaysRun = true)
	   
	   public void beforeClass() {
		   
		 //Create a new instance of the firefox driver
		     driver = new FirefoxDriver();
		     
		   //Open browser  
		   driver.get("https://alchemy.hguy.co/crm/");
		 }

	   @Test
	   
		  public void activity1() {
			   // Check the title of the page
		        String title = driver.getTitle();
		            
		        //Print the title of the page
		        System.out.println("Page title is: " + title);
		            
		            //Assertion for page title
		        Assert.assertEquals("SuiteCRM", title);
		  }
	   
	   
		 @Test(enabled = false)
		  public void activity2() {
				  
			 WebElement imagelink = driver.findElement(By.xpath("/html/body/div[1]/div[1]/a"));
			 System.out.println("URL of the header image is :" + imagelink.getAttribute("href"));
			 
			 }
		  
		 @Test
		  
		  public void activity3() {
			  
			 WebElement footertext = driver.findElement(By.id("admin_options"));
			 
			 System.out.println("first copyright text in the footer :" +footertext.getText());
			
		  }
		  
		  @Test 
		  
		  public void activity4() {
			  
			 driver.findElement(By.id("user_name")).sendKeys("admin");
			 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
			 
			 driver.findElement(By.id("bigbutton")).click();
			 
			// WebElement homepage = driver.findElement(By.id("grouptab_0"));
			 
			//Assert.assertEquals("SALES",homepage.getText());
					  
		  }
		 
		 @Test (priority= 1,  dependsOnMethods = {"activity4"})
		 
		 public void activity5(){
			String colour = driver.findElement(By.id("toolbar")).getCssValue("color");
			Assert.assertEquals("rgb(83, 77, 100)", colour);
			System.out.println("the navigation colour is: " + colour);
			
				
		 }
		 
		 @Test (dependsOnMethods = {"activity4"})
		 
		 public void Activity10() throws InterruptedException {         
			 Thread.sleep(2000);		 
			 WebElement home = driver.findElement(By.xpath("//*[@class='navbar-brand with-home-icon suitepicon suitepicon-action-home']"));
			 home.click();
			 
			 Thread.sleep(2000);
			 
			   List<WebElement> dashlet = driver.findElements(By.xpath("//td[contains(@class, 'dashlet-title')]"));
	              Thread.sleep(2000); 
	     		 System.out.println("The total rows are: " + dashlet.size());
	     		 
	     		for (WebElement title : dashlet) {
	    			 System.out.println("The 6 dashlet title are: " + title.getText());
	     		 
	     		}
		 }
		
  // @AfterClass
  public void afterClass() {
  }

}
