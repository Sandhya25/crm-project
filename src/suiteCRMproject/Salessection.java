package suiteCRMproject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Salessection {
	WebDriver driver;
	
	   @BeforeClass(alwaysRun = true)
	   
	   public void beforeClass() {
		   
		 //Create a new instance of the firefox driver
		     driver = new FirefoxDriver();
		     driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		     
		   //Open browser  
		   driver.get("https://alchemy.hguy.co/crm/");
		   
		   driver.manage().window().maximize();
		 }
  
		
	    @Test (groups = {"SalesSection", "Activities"})
		
	    public void activity4credentials() {
			  
			 driver.findElement(By.id("user_name")).sendKeys("admin");
			 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
			 
			 driver.findElement(By.id("bigbutton")).click();
			 
			// WebElement homepage = driver.findElement(By.id("grouptab_0"));
			 
			//Assert.assertEquals("SALES",homepage.getText());
					  
		  }
	    
	    @Test (dependsOnMethods = {"activity4credentials"}, groups = {"SalesSection"})
	    
	    public void Activity7() throws InterruptedException  { 
	    	           
			 // Thread.sleep(2000);
			 WebElement w2 = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			 actions.moveToElement(w2).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"))).click().build().perform();
//			 WebDriverWait wait = new WebDriverWait (driver, 20);
//			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/tbody/tr[13]/td[10]/span/span")));
			 
			 Thread.sleep(10000);
			 
			 WebElement filter = driver.findElement(By.xpath("//table[contains(@class,'paginationTable')]/tbody/tr/td[1]/ul[4]/li/a"));
			 filter.click(); 
			 
			 WebElement phonetext = driver.findElement(By.xpath("//input[@id = 'phone_advanced']"));
			 phonetext.sendKeys("9886753030");
			 
			 driver.findElement(By.id("search_form_submit_advanced")).click();
			 
			WebElement addinfor = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[1]/div[3]/form[2]/div[3]/table/tbody/tr/td[10]/span/span"));
			addinfor.click();
			Thread.sleep(5000);
			WebElement phonenum = driver.findElement(By.cssSelector("span.phone"));
	         System.out.println("The Phone number is:" + phonenum.getText());
			 
	         WebElement remove = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div/div[3]/form[2]/div[3]/table/thead/tr[2]/td/table/tbody/tr/td[1]/ul[5]/li[1]/a[2]"));
	         remove.click();
			 
//		 WebElement w3 = driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/tbody/tr[13]/td[10]/span/span"));
//			//w3.click();  
//			Thread.sleep(7000);
//			 
//			WebElement s1 = driver.findElement(By.cssSelector("span.phone"));
//		     System.out.println("The Phone number is:" + s1.getText());
//			 
		 }

	    
	    @Test (dependsOnMethods = {"activity4credentials"}, groups = {"SalesSection"})
	    
	    public void Activity11() throws InterruptedException {         
			// Thread.sleep(4000);
			 WebElement sales = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			  //Thread.sleep(4000);
			 actions.moveToElement(sales).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"))).click().build().perform();
			 
			 Thread.sleep(8000);
			 
			 
			 WebElement importleads = driver.findElement(By.xpath("//div[text()='Import Leads']"));
			// Thread.sleep(5000);
			 importleads.click();
			 
		   driver.findElement(By.xpath ("//*[@id='userfile']")).sendKeys("C:\\Users\\SandhyaNareddi\\Desktop\\sample.csv");
		
		  WebElement nextbutton = driver.findElement(By.id("gonext"));
		  nextbutton.click();
		  driver.findElement(By.xpath("//*[@id='gonext']")).click();
		  driver.findElement(By.xpath("//*[@id='gonext']")).click();
		  driver.findElement(By.xpath("//*[@id='importnow']")).click();
		 // Thread.sleep(4000);
		   driver.findElement(By.xpath("//*[@id='finished']")).click(); 
	
			
		  
		  	 }
	    
	    @Test (dependsOnMethods = {"activity4credentials"}, groups = {"SalesSection"})
	    
		 public void Activity9() throws InterruptedException {         
			 Thread.sleep(2000);
			 WebElement sales = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			  Thread.sleep(4000);
			 actions.moveToElement(sales).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"))).click().build().perform();
			 Thread.sleep(4000);
			// int colNum = driver.findElements(By.xpath("//table[@class='list view table-responsive']/tbody/tr")).size();
			 Thread.sleep(4000);
			 //System.out.println("Total number of columns = " + colNum);
			 
			 
			 String beforeXpath = "//table[contains(@class, 'list view table-responsive')]/tbody//tr[";
			 String afterXpath = "]/td[3]/b/a";	
			 
			 for (int i = 1; i <=10; i++) {
				 String actualXpath = beforeXpath+i+afterXpath;
				 WebElement w2 = driver.findElement(By.xpath(actualXpath));
				 System.out.println("The first 10 values of Name column: " + w2.getText());
				 if (w2.getText().equals("Lead5")) {
					 break;	
				}		
				 
			 
       }
			 
			 System.out.println("*********************************************");		 String beforeXpath1 = "//table[contains(@class, 'list view table-responsive')]/tbody//tr[";
			 String afterXpath1 = "]/td[8]/a";		 for (int i = 1; i <=10; i++) {
				 String actualXpath = beforeXpath1+i+afterXpath1;
				 WebElement w3 = driver.findElement(By.xpath(actualXpath));
				 System.out.println("The first 10 values of User column: " + w3.getText());	
				}		}
		 
		 
			 
	    @Test (dependsOnMethods = {"activity4credentials"}, groups = {"SalesSection"})
	    
	    public void Activity8() throws InterruptedException {         
			 //Thread.sleep(4000);
			 WebElement account = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			  actions.moveToElement(account).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Accounts']"))).click().build().perform();

    		 //Thread.sleep(5000);  
           List <WebElement> rows = driver.findElements(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr"));
           //Thread.sleep(5000);
    		 System.out.println("The total rows are: " +rows.size());
    		 
    		 
//    		Iterator <WebElement> i = rows.iterator();
//    		
//           while(i.hasNext()) {
//           	
//               WebElement row = i.next();
//                         
//             System.out.println(row.getText());
             
		 String l1 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[1]/td[3]")).getText();
		  System.out.println("The odd number #1 name: " + l1);
		  String l2 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[3]/td[3]")).getText();
		  System.out.println("The odd number #3 name: " + l2);
		  String l3 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[5]/td[3]")).getText();
		  System.out.println("The odd number #5 name: " + l3);
		  String l4 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[7]/td[3]")).getText();
		  System.out.println("The odd number #7 name: " + l4);
		  String l5 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[9]/td[3]")).getText();
		  System.out.println("The odd number #9 name: " + l5);
             
             
               
           }
	    
	    @Test (dependsOnMethods = {"activity4credentials"}, groups = {"Activities"})
	    
	    public void Activity6() { 
			
			  WebElement w1 = driver.findElement(By.xpath("//a[@id = 'grouptab_3']"));
			  System.out.println("activities tab is displayed : " + w1.isDisplayed());
			  Assert.assertEquals("ACTIVITIES",w1.getText());
				
		}
	    
	    @Test (dependsOnMethods = {"activity4credentials"}, groups = {"Activities"})
		 
	    public void Activity12() throws InterruptedException {         
			 Thread.sleep(2000);
			 
			 WebElement activities = driver.findElement(By.xpath("//*[@id='grouptab_3']"));
			  Actions actions = new Actions(driver);
			  
			  Thread.sleep(4000);
			  
			 actions.moveToElement(activities).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Meetings']"))).click().build().perform();
			 
			 //Thread.sleep(4000);
			 WebElement w2 = driver.findElement(By.xpath("//a[contains(@data-action-name,'Schedule_Meeting')]"));
			  w2.click();
			  Thread.sleep(4000);
			  driver.findElement(By.id("name")).sendKeys("Selenium batch3 meeting2");
			  Thread.sleep(2000);
			  WebElement w3 = driver.findElement(By.id("date_start_date"));
			  w3.clear();
			  w3.sendKeys("03/13/2020");
			  Select s1 = new Select(driver.findElement(By.id("date_start_hours")));
			  s1.selectByVisibleText("11");
			  Select s2 = new Select(driver.findElement(By.id("date_start_minutes")));
			  s2.selectByVisibleText("00");
			  WebElement w6 = driver.findElement(By.id("date_end_date"));
			  w6.clear();
			  w6.sendKeys("03/13/2020");
			  Select s3 = new Select(driver.findElement(By.id("date_end_hours")));
			  s3.selectByVisibleText("12");
			  Select s4 = new Select(driver.findElement(By.id("date_end_minutes")));
			  s4.selectByVisibleText("00");
			  WebElement w9 = driver.findElement(By.id("search_last_name"));
			  w9.clear();	  
		      w9.sendKeys("lead");
		      driver.findElement(By.id("invitees_search")).click();
		      //Thread.sleep(2000);
		      driver.findElement(By.id("invitees_add_1")).click();
		      driver.findElement(By.id("invitees_add_2")).click();
		      driver.findElement(By.id("invitees_add_4")).click();
		      driver.findElement(By.id("SAVE_HEADER")).click();
		      WebElement w10 = driver.findElement(By.xpath("//a[contains(@data-action-name,'List')]"));
			  w10.click();
			  Thread.sleep(5000);			 
			  String beforeXpath = "//table[contains(@class, 'list view table-responsive')]/tbody//tr[";
				 String afterXpath = "]/td[4]/b/a";			
				 for (int i = 1; i <=10; i++) {
					 String actualXpath = beforeXpath+i+afterXpath;
					 WebElement e1 = driver.findElement(By.xpath(actualXpath));
					 //System.out.println("The first 10 values of Name column: " + w2.getText());
					 if (e1.getText().equals("Selenium batch3 meeting2")) {
						 break;	
					 }
				Assert.assertEquals(e1.getText(), "Selenium batch3 meeting2");
				System.out.println("The meeting is scheduled: " + e1.getText());
				 }
				 
		 }   
	    

 // @AfterClass
  public void afterClass() {
  }

}
