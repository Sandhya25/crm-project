package suiteCRMproject;

import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.sun.corba.se.spi.orbutil.fsm.Action;

import javafx.util.Duration;

import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


	public class Project1 {
		
		WebDriver driver;
		
	   @BeforeClass
	   public void beforeClass() {
		   
		 //Create a new instance of the firefox driver
		     driver = new FirefoxDriver();
		     
		   //Open browser  
		   driver.get("https://alchemy.hguy.co/crm/");
		 }

	// @Test(priority=0)
	  public void activity1() {
		   // Check the title of the page
	        String title = driver.getTitle();
	            
	        //Print the title of the page
	        System.out.println("Page title is: " + title);
	            
	            //Assertion for page title
	        Assert.assertEquals("SuiteCRM", title);
	  }
	  
	 //@Test(priority=1)
	  public void activity2() {
			  
		 WebElement imagelink = driver.findElement(By.xpath("/html/body/div[1]/div[1]/a"));
		 System.out.println("URL of the header image is :" + imagelink.getAttribute("href"));
		 
		 }
	  
	// @Test(priority=2)
	  
	  public void activity3() {
		  
		 WebElement footertext = driver.findElement(By.id("admin_options"));
		 
		 System.out.println("first copyright text in the footer :" +footertext.getText());
		
	  }
	  
	@Test(priority=3)
	  
	  public void activity4() {
		  
		 driver.findElement(By.id("user_name")).sendKeys("admin");
		 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		 
		 driver.findElement(By.id("bigbutton")).click();
		 
		// WebElement homepage = driver.findElement(By.id("grouptab_0"));
		 
		//Assert.assertEquals("SALES",homepage.getText());
				  
	  }
	 
	// @Test(priority=4)
	 
	 public void activity5(){
		String colour = driver.findElement(By.id("toolbar")).getCssValue("color");
		Assert.assertEquals("rgb(83, 77, 100)", colour);
		System.out.println("the navigation colour is: " + colour);
		
			
	 }
	 
	// @Test(priority=5)
	 
	 public void Activity6() { 
		
			  WebElement w1 = driver.findElement(By.xpath("//a[@id = 'grouptab_3']"));
			  System.out.println("activities tab is displayed : " + w1.isDisplayed());
			  Assert.assertEquals("ACTIVITIES",w1.getText());
				
		}
	  
	//  @Test(priority=6)
	 
	 public void Activity7() throws InterruptedException {         
		 Thread.sleep(2000);
		 WebElement w2 = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
		  Actions actions = new Actions(driver);
		  
		 actions.moveToElement(w2).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"))).click().build().perform();
		 Thread.sleep(7000);
		 
		 WebElement filter = driver.findElement(By.xpath("//table[contains(@class,'paginationTable')]/tbody/tr/td[1]/ul[4]/li/a"));
		 filter.click(); 
		 
		 WebElement phonetext = driver.findElement(By.xpath("//input[@id = 'phone_advanced']"));
		 phonetext.sendKeys("9886753030");
		 
		 driver.findElement(By.id("search_form_submit_advanced")).click();
		 
		WebElement addinfor = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[1]/div[3]/form[2]/div[3]/table/tbody/tr/td[10]/span/span"));
		addinfor.click();
		Thread.sleep(5000);
		WebElement phonenum = driver.findElement(By.cssSelector("span.phone"));
         System.out.println("The Phone number is:" + phonenum.getText());
		 
         WebElement remove = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div/div[3]/form[2]/div[3]/table/thead/tr[2]/td/table/tbody/tr/td[1]/ul[5]/li[1]/a[2]"));
         remove.click();
		 
         
		 
//		 WebElement w3 = driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/tbody/tr[13]/td[10]/span/span"));
//		w3.click();
//		Thread.sleep(4000);
//		WebElement s1 = driver.findElement(By.cssSelector("span.phone"));
//	     System.out.println("The Phone number is:" + s1.getText());
		 
	 }
		 
		   //@Test(priority=7)
		 
		 public void Activity8() throws InterruptedException {         
			 Thread.sleep(4000);
			 WebElement account = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			  actions.moveToElement(account).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Accounts']"))).click().build().perform();
 
     		 Thread.sleep(5000);  
            List <WebElement> rows = driver.findElements(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr"));
            Thread.sleep(5000);
     		 System.out.println("The total rows are: " +rows.size());
     		 
     		 
     		Iterator <WebElement> i = rows.iterator();
     		
            while(i.hasNext()) {
            	
                WebElement row = i.next();
                          
              System.out.println(row.getText());
              
              
                
            }
		 }
            
             
                  
                
     			 
     		 
////			 String l1 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[1]/td[3]")).getText();
////			  System.out.println("The odd number #1 name: " + l1);
////			  String l2 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[3]/td[3]")).getText();
////			  System.out.println("The odd number #3 name: " + l2);
////			  String l3 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[5]/td[3]")).getText();
////			  System.out.println("The odd number #5 name: " + l3);
////			  String l4 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[7]/td[3]")).getText();
////			  System.out.println("The odd number #7 name: " + l4);
////			  String l5 = driver.findElement(By.xpath("//div[contains(@class, 'list-view-rounded-corners')]/table/tbody/tr[9]/td[3]")).getText();
////			  System.out.println("The odd number #9 name: " + l5);
////			  
		 
		 
        @Test(priority=8)
		 
		 public void Activity9() throws InterruptedException {         
			 Thread.sleep(2000);
			 WebElement sales = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			  Thread.sleep(4000);
			 actions.moveToElement(sales).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"))).click().build().perform();
			 Thread.sleep(4000);
			// int colNum = driver.findElements(By.xpath("//table[@class='list view table-responsive']/tbody/tr")).size();
			 Thread.sleep(4000);
			 //System.out.println("Total number of columns = " + colNum);
			 
			 
			 String beforeXpath = "//table[contains(@class, 'list view table-responsive')]/tbody//tr[";
			 String afterXpath = "]/td[3]/b/a";	
			 
			 for (int i = 1; i <=10; i++) {
				 String actualXpath = beforeXpath+i+afterXpath;
				 WebElement w2 = driver.findElement(By.xpath(actualXpath));
				 System.out.println("The first 10 values of Name column: " + w2.getText());
				 if (w2.getText().equals("Lead5")) {
					 break;	
				}		
				 
			 
        }
			 
			 System.out.println("*********************************************");		 String beforeXpath1 = "//table[contains(@class, 'list view table-responsive')]/tbody//tr[";
			 String afterXpath1 = "]/td[8]/a";		 for (int i = 1; i <=10; i++) {
				 String actualXpath = beforeXpath1+i+afterXpath1;
				 WebElement w3 = driver.findElement(By.xpath(actualXpath));
				 System.out.println("The first 10 values of User column: " + w3.getText());	
				}		}
		 
			 
			 
        
			

//	/html/body/div[2]/div[1]/div/div[3]/form[2]/div[3]/table/tbody/tr[1]/td[8]
//			/html/body/div[2]/div[1]/div/div[3]/form[2]/div[3]/table/tbody/tr[3]/td[8]/a
//			/html/body/div[2]/div[1]/div/div[3]/form[2]/div[3]/table/tbody/tr[4]/td[8]/a
//			 
			
		        
	//  @Test(priority=9)
	 
	 public void Activity10() throws InterruptedException {         
		 Thread.sleep(2000);		 
		 WebElement home = driver.findElement(By.xpath("//*[@class='navbar-brand with-home-icon suitepicon suitepicon-action-home']"));
		 home.click();
		 
		 Thread.sleep(2000);
		 
		   List<WebElement> dashlet = driver.findElements(By.xpath("//td[contains(@class, 'dashlet-title')]"));
              Thread.sleep(2000); 
     		 System.out.println("The total rows are: " + dashlet.size());
     		 
     		for (WebElement title : dashlet) {
    			 System.out.println("The 6 dashlet title are: " + title.getText());
     		 
     		}
	 }
	 
	
	
	
	
	
	
	
	//  @Test(priority=10)
	 
		 public void Activity11() throws InterruptedException {         
			 Thread.sleep(2000);
			 WebElement sales = driver.findElement(By.xpath("//*[@id='grouptab_0']"));
			  Actions actions = new Actions(driver);
			  Thread.sleep(4000);
			 actions.moveToElement(sales).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"))).click().build().perform();
			 
			 Thread.sleep(4000);
			 
			 
			 WebElement importleads = driver.findElement(By.xpath("//div[text()='Import Leads']"));
			 Thread.sleep(2000);
			 importleads.click();
			 
		   driver.findElement(By.xpath ("//*[@id='userfile']")).sendKeys("C:\\Users\\SandhyaNareddi\\Desktop\\sample.csv");
		
		  WebElement nextbutton = driver.findElement(By.id("gonext"));
		  nextbutton.click();
		  driver.findElement(By.xpath("//*[@id='gonext']")).click();
		  driver.findElement(By.xpath("//*[@id='gonext']")).click();
		  driver.findElement(By.xpath("//*[@id='importnow']")).click();
		  Thread.sleep(2000);
		   driver.findElement(By.xpath("//*[@id='finished']")).click(); 
	
			
		  
		  	 }
	
	

  //  @Test(priority=11)
		 
		 public void Activity12() throws InterruptedException {         
			 Thread.sleep(2000);
			 
			 WebElement activities = driver.findElement(By.xpath("//*[@id='grouptab_3']"));
			  Actions actions = new Actions(driver);
			  
			  Thread.sleep(4000);
			  
			 actions.moveToElement(activities).moveToElement(driver.findElement(By.xpath("//a[@id='moduleTab_9_Meetings']"))).click().build().perform();
			 
			 Thread.sleep(4000);
			 WebElement w2 = driver.findElement(By.xpath("//a[contains(@data-action-name,'Schedule_Meeting')]"));
			  w2.click();
			  Thread.sleep(4000);
			  driver.findElement(By.id("name")).sendKeys("Selenium batch3 meeting2");
			  Thread.sleep(2000);
			  WebElement w3 = driver.findElement(By.id("date_start_date"));
			  w3.clear();
			  w3.sendKeys("03/13/2020");
			  Select s1 = new Select(driver.findElement(By.id("date_start_hours")));
			  s1.selectByVisibleText("11");
			  Select s2 = new Select(driver.findElement(By.id("date_start_minutes")));
			  s2.selectByVisibleText("00");
			  WebElement w6 = driver.findElement(By.id("date_end_date"));
			  w6.clear();
			  w6.sendKeys("03/13/2020");
			  Select s3 = new Select(driver.findElement(By.id("date_end_hours")));
			  s3.selectByVisibleText("12");
			  Select s4 = new Select(driver.findElement(By.id("date_end_minutes")));
			  s4.selectByVisibleText("00");
			  WebElement w9 = driver.findElement(By.id("search_last_name"));
			  w9.clear();	  
		      w9.sendKeys("lead");
		      driver.findElement(By.id("invitees_search")).click();
		      Thread.sleep(2000);
		      driver.findElement(By.id("invitees_add_1")).click();
		      driver.findElement(By.id("invitees_add_2")).click();
		      driver.findElement(By.id("invitees_add_4")).click();
		      driver.findElement(By.id("SAVE_HEADER")).click();
		      WebElement w10 = driver.findElement(By.xpath("//a[contains(@data-action-name,'List')]"));
			  w10.click();
			  Thread.sleep(2000);			 String beforeXpath = "//table[contains(@class, 'list view table-responsive')]/tbody//tr[";
				 String afterXpath = "]/td[4]/b/a";			 for (int i = 1; i <=10; i++) {
					 String actualXpath = beforeXpath+i+afterXpath;
					 WebElement e1 = driver.findElement(By.xpath(actualXpath));
					 //System.out.println("The first 10 values of Name column: " + w2.getText());
					 if (e1.getText().equals("Selenium batch3 meeting2")) {
						 break;	
					 }
				Assert.assertEquals(e1.getText(), "Selenium batch3 meeting2");
				System.out.println("The meeting is scheduled: " + e1.getText());
				 }
				 
		 }
		 
	
	
			
			 
			 
    //  @Test(priority=12)
       
		 
		    public void Activity13() throws InterruptedException, IOException, CsvException {         
			 Thread.sleep(2000);
			 
//			 List<WebElement> optionList = driver.findElements(By.xpath("//*[@id='grouptab_5']"));
//			// The option you mentioned in your example is stored at position #17 in an array so either you can manually enter the number or you can find it by running FOR lool
//			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", optionList.get(25) );

 
		WebElement all = driver.findElement(By.xpath("//*[@id='grouptab_5']"));
			  Actions action = new Actions(driver);
			  
			  Thread.sleep(4000);
			  
			 action.moveToElement(all).click().build().perform();
			 
			 Thread.sleep(4000);
			 
			driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[7]/span[2]/ul/li[25]/a")).click();
			Thread.sleep(7000);
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div[1]/ul/li[1]/a/div[2]")).click();
		
			CSVReader reader = new CSVReader(new FileReader("src/resources/sample1.csv")); 
			
			List<String[]> list = reader.readAll();
	        System.out.println("Total number of rows are: " + list.size());
	        
	      //Create Iterator reference
	        Iterator<String[]> itr = list.iterator();

	        //Iterate all values
	        while(itr.hasNext()) {
	            String[] str = itr.next();

	            System.out.print("Values are: ");
	        
	        for(int i=0;i<str.length;i++){
	        	String PName = str[i];
	        	String PartNumber =str[i+1];
	        	String Cost =str[i+2];
	        	String Price =str[i+3];
	        	
	        	System.out.println("product name is :" +PName);
	        	System.out.println("product name is :" +PartNumber);
	        	System.out.println("product name is :" +Cost);
	        	System.out.println("product name is :" +Price);
	        	
	        	Thread.sleep(7000);
	        	
	        	
	        	driver.findElement(By.xpath("//*[@id='name']")).sendKeys("PName");
	        	driver.findElement(By.id("part_number")).sendKeys("PartNumber");
	        	driver.findElement(By.id("cost")).sendKeys("Cost");
	        	driver.findElement(By.id("price")).sendKeys("Price");
	        	
	        	Thread.sleep(3000);
	        	driver.findElement(By.xpath("//*[@id='save_and_continue']")).click();
	        }
	        }
//	        	
	        	
	        
	       
//			 
//			// Create instance of Javascript executor
//			 JavascriptExecutor je = (JavascriptExecutor) driver;
//			 //Identify the WebElement which will appear after scrolling down
//			 WebElement element = driver.findElement(By.tagName("...."));
//			 // now execute query which actually will scroll until that element is not appeared on page.
//			 je.executeScript("arguments[0].scrollIntoView(true);",element);
//			 
			 
	
	        
	        
			}
//				  
				  
			  
			  
			  

			 
			 
       }
       
	
	
			 
			 
	
	
 
 
	
	
	
				 
		 
	        
	        
		 
		 
		 
		 
		 
 

		 
		 
		 
	
		 
			 
			  
			 
			 
		 
		 
		
		

		 
		

		 	 
		 
	 
	  
	//  @AfterClass
	 // public void afterClass() {
		 // driver.close();
			 
			 
      //  }
	  
	  
	  
	