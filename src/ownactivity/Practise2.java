package ownactivity;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Practise2 {

	public static void main(String[] args) {

	 /*  	        WebDriver driver = new FirefoxDriver();
	        
	        //Open browser
	        driver.get("https://training-support.net");
	        
	        //Use linkText() to find <a> tag
	        WebElement anchorTag = driver.findElement(By.linkText("About Us"));
	        //Use getText() to get innerHTML text
	        String anchorText = anchorTag.getText();
	        //Print the String
	        System.out.println("Anchor tag text is: " + anchorText);
	        
	        //Close browser
	        driver.close();
	        
	        */
		
		
		 WebDriver driver = new FirefoxDriver();
	        
	        //Open browser
	        driver.get("http://www.google.com");
	    	//WebElement searchbox = driver.findElement(By.name("q"));
			
			
			WebElement searchbox = driver.findElement(By.xpath("//input[@class= 'gLFyf gsfi']"));
			
			searchbox.sendKeys("cheese");
			
			driver.findElement(By.xpath("/html/body/div/div[3]/form/div[2]/div[1]/div[3]/center/input[1]")).click();
			
			WebElement matchingResult= driver.findElement(By.xpath(".//div[@class='aajZCb']/ul"));
			List<WebElement> listResult= matchingResult.findElements(By.xpath("//li/div/div[@class='sbtc']"));
			System.out.println(listResult.size());
			//if you want to print matching results
			     for(WebElement results: listResult)
			       {
			         String value= results.getText();
			         System.out.println(value);
			       }     
	    }

	}




