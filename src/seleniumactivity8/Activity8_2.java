package seleniumactivity8;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity8_2 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://training-support.net/selenium/ajax");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is :" +pagetitle);
		
		WebElement button  = driver.findElement(By.cssSelector("button.ui.violet.button"));
		button.click();
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@id = 'ajax-content']/h1"), "HELLO!"));
		
		
		
		
		
		
		
		
		
	}

}
