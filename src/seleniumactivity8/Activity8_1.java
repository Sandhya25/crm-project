package seleniumactivity8;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity8_1 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://training-support.net/selenium/dynamic-controls");
		
		String pagetitle = driver.getTitle();
	
		System.out.println("The page title is :" +pagetitle);
		
		WebElement checkbox = driver.findElement(By.name("toggled"));
		WebElement removecheckbox = driver.findElement(By.id("toggleCheckbox"));
		removecheckbox.click();
		
			
		WebDriverWait wait = new WebDriverWait(driver, 10);	
		
		//Wait for checkbox to disappear
        wait.until(ExpectedConditions.invisibilityOf(checkbox));
        
        removecheckbox.click();
        
      //Wait for checkbox to appear
        wait.until(ExpectedConditions.visibilityOf(checkbox));
        checkbox.click();
        
      //Close browser
        driver.close();
		
		

	}

}
