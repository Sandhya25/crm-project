package seleniumactivity4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4_1 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get ("https://www.training-support.net");
				
		String pagetitle = driver.getTitle();
		System.out.println("the title of the page is:" +pagetitle);
		
		WebElement linkonpage = driver.findElement(By.xpath("/html/body/div/div[1]/div/a"));
		
		linkonpage.click();
		
				
		String Newpagetitle = driver.getTitle();
		System.out.println("the title of the new page is:" +Newpagetitle);
		
		driver.close();
		
		
		
		

	}

}
