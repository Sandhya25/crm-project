package seleniumactivity3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3 {

	public static void main(String[] args) {
		
	WebDriver driver = new FirefoxDriver();
	
	driver.get("https://training-support.net/selenium/simple-form");
	
	String pagetitle = driver.getTitle();
	   
	System.out.println("the pge of the title is: " +pagetitle);
	
    //Find the input fields
	
    WebElement firstName = driver.findElement(By.id("firstName"));
    
    WebElement lastName = driver.findElement(By.id("lastName"));
    
  //Enter text
    firstName.sendKeys("Sandhya");
    lastName.sendKeys("Nareddi");
    
    //WebElement email = driver.findElement(By.id("email"));
    
    driver.findElement(By.id("email")).sendKeys("abc@gmail.com");
    
    WebElement contactnumber = driver.findElement(By.id("number"));
    
    //email.sendKeys("sandhyar_a@yahoo.com");
    contactnumber .sendKeys("9886745635");
    
    //Click Submit
    driver.findElement(By.id("simpleForm")).submit();

    //Close the browser
    driver.close();
    
    
    
	

	}

}
