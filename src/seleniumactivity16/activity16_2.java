package seleniumactivity16;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class activity16_2 {
	
	WebDriver driver;
	
	@BeforeClass
	public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("https://www.training-support.net/selenium/login-form");
    }
	
	@Test(priority=0)
	
	public void successmethod() {
		
	
	WebElement username1 = driver.findElement(By.id("username"));
	username1.sendKeys("admin");
	
	WebElement password1 = driver.findElement(By.id("password"));
	password1.sendKeys("password");
	
	driver.findElement(By.xpath("//button[text()='Log in']")).click();
	
	WebElement successmessage = driver.findElement(By.id("action-confirmation"));
	
	System.out.println("login message is :" +successmessage.getText());
		
	Assert.assertEquals(successmessage.getText(), "Welcome Back, admin");
	}
	
	@Test(priority=1)
	public void failuremethod() {
		
		
		WebElement username2 = driver.findElement(By.id("username"));
		username2.clear();
		
		username2.sendKeys("admin");
		
		WebElement password2 = driver.findElement(By.id("password"));
		password2.clear();
		password2.sendKeys("PASSWORD");
		
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		
		WebElement failuremessage = driver.findElement(By.id("action-confirmation"));
		
		System.out.println("login message is :" +failuremessage.getText());
			
		Assert.assertEquals(failuremessage.getText(), "Invalid Credentials");
		}
	
	// @AfterTest
	   // public void afterMethod() {
	        //Close the browser
	       // driver.close();
	  //  }
	
	
	
	
	

	
	
	
	

}
