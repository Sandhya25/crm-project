package seleniumactivity16;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activity16_3 {
	
WebDriver driver;
	
	@BeforeClass
	public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("https://training-support.net/selenium/dynamic-controls");
    }
	
	@Test
	
	public void checkboxappear() {
		
		WebElement checkboxInput = driver.findElement(By.xpath("//input[@type='checkbox']"));
		
        System.out.println("The checkbox Input is displayed: " + checkboxInput.isDisplayed());
        
        Assert.assertTrue(checkboxInput.isDisplayed());
        
	}
	
	@Test
	
	public void checkboxdisappear() {
		
		WebElement checkboxInput = driver.findElement(By.xpath("//input[@type='checkbox']"));
	
       driver.findElement(By.id("toggleCheckbox")).click();
       System.out.println("The checkbox Input is displayed: " + checkboxInput.isDisplayed());
       Assert.assertFalse(checkboxInput.isDisplayed());
		
		
	}

}
