package seleniumactivity5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5_3 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://training-support.net/selenium/dynamic-controls");
		
		String pagetitle = driver.getTitle();
		
		System.out.println("Page title is :" +pagetitle);
		
		WebElement result1 = driver.findElement(By.id("input-text"));
		
		System.out.println("textfield is enabled :" +result1.isEnabled());
		
		driver.findElement(By.id("toggleInput")).click();
		System.out.println("textfield is enabled :" +result1.isEnabled());
		
		driver.close();
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}

}
