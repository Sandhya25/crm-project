package seleniumactivity5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5_2 {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.get(" https://training-support.net/selenium/dynamic-controls");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is: " +pagetitle);
		
		WebElement checkboxinput = driver.findElement(By.xpath("//input[@type ='checkbox']"));
		
		System.out.println("Firsttime input is :" +checkboxinput.isSelected());
		
		checkboxinput.click();
			
		
		System.out.println("the second result is :" +checkboxinput.isSelected());
		
		

	}

}
