package weekendactivity3;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity3 {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		driver.get("https://www.amazon.in/");
		
		String pagetitle = driver.getTitle();
		System.out.println("The page title is :" + pagetitle);
		
		WebElement searchbox = driver.findElement(By.id("twotabsearchtextbox"));
		searchbox.sendKeys("samsung");
		WebElement searchicon = driver.findElement(By.cssSelector("input"));
		searchicon.click();
		//wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("resultpage")));
		//driver).window().maximize();
		
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[2]/div/span[4]/div[1]/div[1]/div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span")).click();
		
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("add-to-cart-button")));
		Thread.sleep(10000);
		
        //wait.until(ExpectedConditions.numberOfWindowsToBe(2));
	
         //Get Window handles
	
        Set<String> allWindowHandles = driver.getWindowHandles();	
        System.out.println("All window handles: " + allWindowHandles);
        
        //Loop through each handle
    	
        for (String handle : driver.getWindowHandles()) {
	
            driver.switchTo().window(handle);
	
        }
        driver.findElement(By.id("add-to-cart-button")).click();
        
       
        
        Thread.sleep(10000);
        driver.close();
        
       
        
       
	
		
		
		
		
		
		
		
		
		

	}

}
